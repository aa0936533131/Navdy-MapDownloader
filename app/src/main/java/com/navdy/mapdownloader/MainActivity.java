/*
 * Copyright (c) 2011-2018 HERE Europe B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.navdy.mapdownloader;

import android.Manifest;
import android.app.ListActivity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.usb.UsbManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.stream.Collectors;


import static android.preference.PreferenceManager.getDefaultSharedPreferences;

/**
 * Main activity which launches the map list view and handles Android run-time requesting
 * permission.
 */
public class MainActivity extends ListActivity {
    private static final int REQUEST_CODE_ASK_PERMISSIONS = 1;
    private static final String[] RUNTIME_PERMISSIONS = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.INTERNET,
            Manifest.permission.ACCESS_WIFI_STATE,
            Manifest.permission.ACCESS_NETWORK_STATE
    };
    public MapListView m_mapListView;
    private final String maps_path_key = "maps_path";
    private SharedPreferences m_preferences;
    private Boolean isNavdyDetected = false;
    private FileDialog fileDialog = null;

    public class MountingPath {
        String name;
        String path;

        public MountingPath(String name, String path) {
            this.name = name;
            this.path = path;
        }

        public String description() {
            return this.name + ": " + this.path;
        }
    }

    private String last_used = null;

    public ArrayList<MountingPath> common_paths () {
        ArrayList<MountingPath> paths = new ArrayList<MountingPath>();

        if (last_used == null) {
            String previous = m_preferences.getString(maps_path_key, "");
            if (!"".equals(previous)) {
                paths.add(0, new MountingPath("Last Used", previous));
            }
        } else {
            paths.add(new MountingPath("Last Used", last_used));
        }

        paths.add(new MountingPath("KOPlayer shared drive", "/mnt/asar/share"));
        paths.add(new MountingPath("Navdy Nox Mounter", "/mnt/shared/Other/Navdy"));

        if (Build.PRODUCT.equals("android_x86")) {
            // listen for new devices
            // BroadcastReceiver when remove the device USB plug from a USB port
            BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {
                public void onReceive(Context context, Intent intent) {
                    String action = intent.getAction();
                    if (UsbManager.ACTION_USB_DEVICE_ATTACHED.equals(action)) {
                        final Handler handler = new Handler();

                        final Runnable showDirectoryBrowserSoon = new Runnable() {
                            @Override
                            public void run() {
                                showDirectoryBrowser();
                            }
                        };

                        final Runnable monitor_mount = new Runnable() {
                            String current_mount = null;
                            @Override
                            public void run() {
                                try {
                                    String lines = "";
                                    String line;
                                    Process p = Runtime.getRuntime().exec("/system/bin/mount");
                                    BufferedReader input = new BufferedReader
                                                    (new InputStreamReader(p.getInputStream()));
                                    while ((line = input.readLine()) != null) {
                                        lines = lines.concat(line);
                                    }
                                    input.close();
                                    if (current_mount == null) {
                                        current_mount = lines;
                                    }
                                    if (!lines.equals(current_mount)) {
                                        // Restart the DirectoryBrowser
                                        handler.postDelayed(showDirectoryBrowserSoon, 1000);
                                    } else {
                                        handler.postDelayed(this, 500);
                                    }
                                }
                                catch (Exception err) {
                                    err.printStackTrace();
                                }
                            }
                        };
                        handler.postDelayed(monitor_mount, 1000);
                    }
                }
            };

            IntentFilter filter = new IntentFilter();
            filter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
            registerReceiver(mUsbReceiver, filter);

            File storage = new File("/storage");
            File[] filesList = storage.listFiles();
            for (File entry : filesList) {
                if (!entry.isFile()) {
                    if (!entry.getName().equals("self") &&
                            !entry.getName().startsWith("sdcard") &&
                            !entry.getName().equals("emulated")) {
                        File[] contents = entry.listFiles();
                        if ((contents != null) && (contents.length > 0)) {
                            paths.add(0, new MountingPath("External drive", entry.getAbsolutePath()));
                        }
                    }
                }
            }
        }

        return paths;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        m_preferences = getDefaultSharedPreferences(this);

        m_mapListView = new MapListView(this);

        if (hasPermissions(this, RUNTIME_PERMISSIONS)) {
            initMaps();
        } else {
            ActivityCompat.requestPermissions(this, RUNTIME_PERMISSIONS, REQUEST_CODE_ASK_PERMISSIONS);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (UsbManager.ACTION_USB_ACCESSORY_ATTACHED.equals(intent.getAction())) {
            LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
        }
    }


    /**
     * Only when the app's target SDK is 23 or higher, it requests each dangerous permissions it
     * needs when the app is running.
     */
    private static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
            @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS: {
                for (int index = 0; index < permissions.length; index++) {
                    if (grantResults[index] != PackageManager.PERMISSION_GRANTED) {

                        /*
                         * If the user turned down the permission request in the past and chose the
                         * Don't ask again option in the permission request system dialog.
                         */
                        if (!ActivityCompat.shouldShowRequestPermissionRationale(this,
                                permissions[index])) {
                            Toast.makeText(this,
                                    "Required permission " + permissions[index] + " not granted. "
                                            + "Please go to settings and turn on for sample app",
                                    Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(this,
                                    "Required permission " + permissions[index] + " not granted",
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                }

                initMaps();
                break;
            }
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public void initMaps() {

        String maps_path = "";
        Boolean pathIsDirectory = false;

        for (MountingPath mount: common_paths()) {
            try{ pathIsDirectory = (new File(mount.path)).isDirectory(); } catch (Exception ex) { }
            if (pathIsDirectory) {
                maps_path = setDownloadPath(mount.path);
                break;
            }
        }
        // No known paths exist, ask the user to choose
        if(!pathIsDirectory) {
            showDirectoryBrowser();
            return;
        }
        detectNavdy(maps_path);

        // All permission requests are being handled. Create map fragment view. Please note
        // the HERE SDK requires all permissions defined above to operate properly.
        m_mapListView.initMapEngine(maps_path, isNavdyDetected);
    }

    public String setDownloadPath(String path) {
        if (! path.endsWith("/.here-maps")) {
            path = path.concat("/.here-maps");
            try {
                (new File(path)).mkdirs();
            } catch (Exception ex) {
            }
        }
        SharedPreferences.Editor editor = m_preferences.edit();
        editor.putString(maps_path_key, path);
        last_used = path;
        editor.apply();
        return path;
    }

    public void detectNavdy(String path) {
        if (path.endsWith("/.here-maps")) {
            path = path.replace("/.here-maps", "");
        }

        try{
            isNavdyDetected = (new File(path.concat("/gesture_videos"))).isDirectory();
        }catch (Exception ex) {
            isNavdyDetected = false;
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        m_mapListView.onListItemClicked(l, v, position, id);
    }

    public void showDirectoryBrowser() {
        File mPath = new File(Environment.getExternalStorageDirectory() + "//");
        if (fileDialog == null) {
            fileDialog = new FileDialog(this, mPath, null);
        }
        fileDialog.close();
        fileDialog.addDirectoryListener(new FileDialog.DirectorySelectedListener() {
            public void directorySelected(File directory) {
                String path = directory.toString();
                Log.d(getClass().getName(), "selected dir " + path);
                setDownloadPath(path);
                initMaps();
            }
        });
        fileDialog.setSelectDirectoryOption(true);
        fileDialog.showShortCutsDialog();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode) {
            case 1234:
                Log.i("Test", "Result URI " + data.getData());
                SharedPreferences.Editor editor = m_preferences.edit();
                try {
                    editor.putString(maps_path_key, data.getData().getPath());
                    last_used = data.getData().getPath();
                    editor.apply();
                    initMaps();
                } catch (NullPointerException ex) {
                    showDirectoryBrowser();
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if( m_mapListView.upperLevel() == false ){
            super.onBackPressed();
        }
    }
}
