# run with
#> docker run -it --privileged -e MEGA_USER -e MEGA_PASS -v $PWD:/root/tmp butomo1989/docker-android-x86-5.1.1 bash ./tmp/download_all.sh

if [ -z "$IN_MAPDOWNLOADER" ]; then
  docker run -it --privileged -e MEGA_USER -e MEGA_PASS -e IN_MAPDOWNLOADER=1 -v $PWD:/root/tmp butomo1989/docker-android-x86-5.1.1 bash ./tmp/download_all.sh
  exit
fi

if [ ! -z "$CI_PROJECT_DIR" ]; then
  export PROJECT_DIR="$CI_PROJECT_DIR"
else
  export PROJECT_DIR="/root/tmp"
fi

cd /root

avdmanager create avd -f -n Nexus_5_5.1 -b default/x86 -c 10000M -k "system-images;android-22;default;x86" -d Nexus\ 5 -p /root/android_emulator

emulator/emulator @Nexus_5_5.1 -gpu guest -verbose -wipe-data -no-window &

function wait_emulator_to_be_ready () {
  boot_completed=false
  while [ "$boot_completed" == false ]; do
    status=$(adb wait-for-device shell getprop sys.boot_completed | tr -d '\r')
    echo "Boot Status: $status"

    if [ "$status" == "1" ]; then
      boot_completed=true
    else
      sleep 3
    fi
  done;
}

if [ ! -z "$MEGA_USER" ]; then
  wget https://megatools.megous.com/builds/experimental/megatools-1.11.0-git-20180814-linux-x86_64.tar.gz;
  tar xvf megatools-1.11.0-git-20180814-linux-x86_64.tar.gz;
  export PATH=$PATH:`pwd`/megatools-1.11.0-git-20180814-linux-x86_64;
  megatools mkdir -u "$MEGA_USER" -p "$MEGA_PASS" /Root/navdy_maps 2> /dev/null;
fi

wait_emulator_to_be_ready

cd "$PROJECT_DIR"
#./gradlew connectedAndroidTest  -DmapsPath=/sdcard/Australia -Dregion="Australia/Oceania"

./gradlew installDebug installDebugAndroidTest
# get region list
adb shell am instrument -w com.navdy.mapdownloader.test/android.support.test.runner.AndroidJUnitRunner
# adb shell am instrument -w -e region Australia/Oceania -e mapsPath /sdcard/Australia com.navdy.mapdownloader.test/android.support.test.runner.AndroidJUnitRunner

function download () {
  pushd "$PROJECT_DIR"
  adb shell am instrument -w -e region "$1" -e mapsPath "/sdcard/$2" com.navdy.mapdownloader.test/android.support.test.runner.AndroidJUnitRunner
  echo "Copy maps from emulator..."
  adb pull "/sdcard/$2" "./$2" > /dev/null
  cd "$2"
  VERSION=$(cat ./.here-maps/version)
  echo "Zip maps to $2_$VERSION.zip"
  zip -q -r "../$2_$VERSION.zip" .here-maps
  cd ..
  rm -rf "$2"
  chmod 666 "$2_$VERSION.zip"
  adb shell rm -rf "/sdcard/$2"

  if [ ! -z "$MEGA_USER" ]; then echo "Uploading "$2_$VERSION.zip" to mega"; megatools put -u "$MEGA_USER" -p "$MEGA_PASS" --path /Root/navdy_maps "$2_$VERSION.zip" 2> /dev/null; fi
  popd
}

download "North\ and\ Central\ America" "North_and_Central_America"
download "South\ America"               "South_America"
download "Europe"                       "Europe"
download "Africa"                       "Africa"
download "Asia"                         "Asia"
download "Australia/Oceania"            "Australia_Oceania"
